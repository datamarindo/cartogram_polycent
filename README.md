**Script de R para crear un tipo de cartograma basado en subdividir el área de un polígono geoespacial**

```
Para correr en RStudio: 
library(shiny)
runUrl("https://gitlab.com/datamarindo/cartogram_polycent/raw/master/shiny_polycent.zip")
# si no funciona a la primera intentar correr la línea una segunda vez

#librerías necesarias install.packages("libreria_faltante")
library(shiny)
library(units)
library(sf)
library(dplyr)
library(raster)
library(ggplot2)

```

<img src="https://gitlab.com/datamarindo/cartogram_polycent/raw/master/tamps.png" width=380 height=290>
    
    Un cartograma es la representación de un atributo mapeada en una variable geoespacial, 
    como área o longitud; en este caso, subdividimos un polígono de límites estatales de México 
    en áreas de acuerdo al porcentaje de cada categoría de una variable.
    Está diseñado para ser meramente una representación visual comparable a un gráfico de pay (pie-chart)
    con la única ventaja de representar un polígono y así brindar una visualización 
    más novedosa.
    
    El algoritmo que diseñé genera una secuencia de latitudes mínimas, que van aumentando a cada iteración
    calculando la razón del área del polígono secundario entre la del polígono original hasta que 
    la última iteración supera el porcentaje de la variable asignable al polígono secundario.
    Este proceso puede requerir de un número elevado de iteraciones (hasta 80) y se vuelve lento en
    polígonos complejos (aquellos con islas y bordes muy irregulares). Es posible mejorar el algoritmo,
    pero depende de que la herramienta sea útil.
    
    Los mapas cargados son de Global Administrative Boundaries a través de la librería raster, por el 
    momento solo hay a nivel estatal para México. Pero con pocas modificaciones al código se puede usar
    para otros países y a otros niveles administrativos.
    

# para citar

```
@software{cartogram,
  author = {Lagunes-Díaz, Elio},
  month = {12},
  title = {{Cartogram polycent, a Shiny app for geospatially related data visualization}},
  url = {https://gitlab.com/datamarindo/cartogram_polycent},
  version = {0.1},
  year = {2020}
}
```
