library(shiny)
library(units)
library(sf)
library(dplyr)
library(raster)
library(ggplot2)

server = function(input, output) {
  observeEvent(input$go, {
  output$map2 <- renderPlot({
    polycent <- function(poly, porcientos) {
      df   <- st_sf(id = 1:length(porcientos), crs = 4326, # empty sf for populating
                    geometry = st_sfc(lapply(1:length(porcientos), function(x) st_multipolygon())))
      area1 <- st_area(poly)  %>% sum() # st_area takes multipolygons as one; # area1 is constant
      poly2 <- poly # duplicating for the final cut
      for(j in  seq_along(porcientos[-length(porcientos)])) { 
        bb = st_bbox(poly2)
        top <- bb['ymax']
        bot <- bb['ymin']
        steps <- seq(bot, top, by = (top - bot) / 80)
        for(i in steps[2:length(steps)]) {  # 2:n because 1:n renders a line "ymax" = "ymin"
          bf <- bb
          bf['ymax'] = i
          temp <- st_intersection(poly, st_as_sfc(bf, 4326))
          area2 <- st_area(temp) %>% sum()
          if(drop_units(area2)/drop_units(area1) >= porcientos[j]) break
          df$geometry[j] <- st_geometry(temp)
        }
        poly2 <- st_difference(poly2, st_union(df))
      }
      df$geometry[length(porcientos)] <- st_geometry(st_difference(poly, st_union(df)))
      poly <- df
      
    }
    
    adm <- raster::getData('GADM', country='MEX', level=1)
    mar <- adm
    ea <- polycent(poly = st_as_sf(mar[mar$NAME_1 == input$state,]),
                   porcientos = as.numeric(unlist(strsplit(input$vec1,","))))#,
                   #valor = input$vec2)
    ea$valor <- as.character(unlist(strsplit(input$vec2, ",")))
    ggplot(ea) + geom_sf(aes(fill = as.factor(id))) + 
      coord_sf(crs = st_crs(ea), datum = NA) +
      scale_fill_manual(values = trimws(as.character(unlist(strsplit(input$colores, ",")))) , guide = F) + 
      geom_sf_label(aes(label = valor), size = 3) + 
      theme(
        panel.ontop = TRUE, 
        panel.grid = element_blank(), 
        line = element_blank(), 
        rect = element_blank()) + 
      labs(title = paste(input$vec3, input$state)) + 
      xlab("") + ylab("") 
  })
  })
  output$result <- renderText({
    paste("Seleccionado:", input$state)
  })
}
