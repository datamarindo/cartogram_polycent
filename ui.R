ui = fluidPage(
  actionButton("go", "Crear cartograma polycent (puede tardar varios segundos)"),
  selectInput("state", "Seleccione un estado:",
              list(`Estado` = list('Aguascalientes', 'Baja California Sur', 
    'Baja California', 'Campeche', 'Chiapas', 'Chihuahua', 'Coahuila', 'Colima', 'Distrito Federal', 
    'Durango', 'Guanajuato', 'Guerrero', 'Hidalgo', 'Jalisco', 'México', 'Michoacán', 'Morelos',
    'Nayarit', 'Nuevo León', 'Oaxaca', 'Puebla', 'Querétaro', 'Quintana Roo', 'San Luis Potosí', 
    'Sinaloa', 'Sonora', 'Tabasco', 'Tamaulipas', 'Tlaxcala', 'Veracruz', 'Yucatán', 'Zacatecas'))
  ),
  textOutput("result"),
  sidebarPanel(
 textInput('vec1', 'Porcentajes (fracción, separada por coma)', "0.4,0.3,0.2,0.1"),
  textInput('vec2', 'Nombres para las etiquetas (mismo número que porcentajes)', 
            'Tipo 1: 40%, Tipo 2: 30%, Tipo 3: 20%, Tipo 4: 10%'),
 textInput('vec3', 'Título del cartograma', 'Variable X en:'),
 textInput('colores', 'Colores (revisar http://www.sthda.com/english/wiki/colors-in-r)',
           'lightcoral, #FFCCCC, paleturquoise3, cornsilk3')),
    mainPanel(fluid = TRUE, plotOutput('map2'))
)

